import React, {useState} from "react";
import styles from "./Accordion.module.css";
import Link from "next/link";

const Accordion = () => {
    const [activeIndex, setActiveIndex] = useState(-1);

    const toggleAccordion = (index: number) => {
        setActiveIndex(index === activeIndex ? -1 : index);
    };

    return (
        <div className={styles.accordion}>
            <div className={styles.accordionItem}>
                <div className={`${styles.accordionHeader} ${activeIndex === 0 ? styles.active : ""}`}
                     onClick={() => toggleAccordion(0)}>
                    <p>Product</p>
                    <span className={`${styles.arrow} ${activeIndex === 0 ? styles.up : ""}`}></span>
                </div>
                <div className={`${styles.accordionContent} ${activeIndex === 0 ? styles.show : ""}`}>
                    <ul>
                        <Link href={'/'}><li>Learn</li></Link>
                        <Link href={'/'}><li>Plugins</li></Link>
                        <Link href={'/'}><li>Gallery</li></Link>
                        <Link href={'/'}><li>Pricing</li></Link>
                    </ul>
                </div>
            </div>
            <div className={styles.accordionItem}>
                <div className={`${styles.accordionHeader} ${activeIndex === 1 ? styles.active : ""}`}
                     onClick={() => toggleAccordion(1)}>
                    <p>Information</p>
                    <span className={`${styles.arrow} ${activeIndex === 1 ? styles.up : ""}`}></span>
                </div>
                <div className={`${styles.accordionContent} ${activeIndex === 1 ? styles.show : ""}`}>
                    <ul>
                        <Link href={'/'}><li>Privacy</li></Link>
                        <Link href={'/'}><li>Cookies</li></Link>
                        <Link href={'/'}><li>Terms</li></Link>
                        <Link href={'/'}><li>Security</li></Link>
                    </ul>
                </div>
            </div>
            <div className={styles.accordionItem}>
                <div className={`${styles.accordionHeader} ${activeIndex === 2 ? styles.active : ""}`}
                     onClick={() => toggleAccordion(2)}>
                    <p>Support</p>
                    <span className={`${styles.arrow} ${activeIndex === 2 ? styles.up : ""}`}></span>
                </div>
                <div className={`${styles.accordionContent} ${activeIndex === 2 ? styles.show : ""}`}>
                    <ul>
                        <Link href={'/'}><li>Updates</li></Link>
                        <Link href={'/'}><li>Discord</li></Link>
                        <Link href={'/'}><li>Startups</li></Link>
                        <Link href={'/'}><li>Contact</li></Link>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Accordion;
