import React, {useState} from "react";
import Link from "next/link";
import styles from "./NavBar.module.css";

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };

    return (
        <nav className={styles.navbar}>
            <div>
                <Link href="/">
                    <img src="/Logo.svg" alt=""/>
                </Link>
            </div>
            <div className={`${styles.menuIcon} ${isOpen ? styles.open : ""}`} onClick={toggleMenu}>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ul className={`${styles.menu} ${isOpen ? styles.open : ""}`}>
                <li>
                    <Link href="/">
                        Home
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        Shop
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        Product
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        Pages
                    </Link>
                </li>
            </ul>
            <ul className={styles.menu}>
                <li>
                    <Link href="/">
                        <img src="/navbar_icons/magni.svg" alt=""/>
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        <img src="/navbar_icons/Union.svg" alt=""/>
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        <img src="/navbar_icons/Vector.svg" alt=""/>
                    </Link>
                </li>
                <li>
                    <Link href="/">
                        <img src="/navbar_icons/Icon%20Cart.svg" alt=""/>
                    </Link>
                </li>
            </ul>
        </nav>
    );
};

export default Navbar;
