import React from 'react';
import styles from './Footer.module.css';
import Link from 'next/link';
import Accordion from "@/components/Accordion/Accordion";
import Socials from '@/components/Socials/Socials'

interface FooterProps {

}

const Footer = (props: FooterProps) => {
    return (
        <footer className={styles.footerSection}>
            <div>
                <div className={styles.footerContent}>
                    <div>
                        <div className={styles.textBlock}>
                            <img src="/Logo.svg" alt=""/>
                            <p>Phosf Luorescently engage worldwide method process shopping.</p>
                            <div className={styles.socialMobile}>
                                <Socials/>
                            </div>
                        </div>
                    </div>
                    <div className={styles.footerColumns}>
                        <div>
                            <ul>
                                <li id={styles.title}>Product</li>
                                <Link href={'/'}>
                                    <li>Learn</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Plugins</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Gallery</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Pricing</li>
                                </Link>
                            </ul>
                        </div>
                        <div>
                            <ul>
                                <li id={styles.title}>Information</li>
                                <Link href={'/'}>
                                    <li>Privacy</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Cookies</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Terms</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Security</li>
                                </Link>
                            </ul>
                        </div>
                        <div>
                            <ul>
                                <li id={styles.title}>Support</li>
                                <Link href={'/'}>
                                    <li>Updates</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Discord</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Startups</li>
                                </Link>
                                <Link href={'/'}>
                                    <li>Contact</li>
                                </Link>
                            </ul>
                        </div>
                    </div>
                    <Accordion/>
                </div>
            </div>
            <div className={styles.bottomOfthebottom}>
                <img className={styles.designer} src="/Socials/©%202088%20Nayzak.svg" alt=""/>
                <div className={styles.curSocials}>
                    <div style={{display: 'flex', gap: '15px'}}>
                        <div style={{display: 'flex', gap: '5px'}}>
                            <img style={{order: '0'}} src="/Selectors/Flags.svg" alt=""/>
                            <Link href={'/'}>
                                <div style={{order: '1'}}>English</div>
                            </Link>
                        </div>
                        <Link href={'/'}>
                            <div>
                                USD
                            </div>
                        </Link>
                    </div>
                    <div className={styles.socialsDesk}><Socials/></div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;

