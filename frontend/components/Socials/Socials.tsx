import React, { FC } from 'react';
import styles from './Socials.module.css';
import Link from "next/link";

interface SocialsProps {}

const Socials: FC<SocialsProps> = () => (

    <div className={styles.socials}>

        <Link href={'/'}><img src="/Socials/Icon%20Box.svg" alt=""/></Link>
        <Link href={'/'}><img src="/Socials/Icon%20Box-1.svg" alt=""/></Link>
        <Link href={'/'}><img src="/Socials/Icon%20Box-2.svg" alt=""/></Link>
        <Link href={'/'}><img src="/Socials/Icon%20Box-3.svg" alt=""/></Link>
    </div>
);

export default Socials;
